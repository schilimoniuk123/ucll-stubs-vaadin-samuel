package be.ucll.java.ent.view;

import be.ucll.java.ent.controller.LeermoduleEJBLocal;
import be.ucll.java.ent.domain.LeermoduleDTO;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

@Route("leermodules")
@Theme(value = Lumo.class, variant = Lumo.DARK)
@PageTitle("StuBS")
public class LeermodulesView extends VerticalLayout implements AfterNavigationObserver {
    @EJB
    private LeermoduleEJBLocal leermoduleEJBLocal;

    private SplitLayout splitLayout;
    private VerticalLayout c1;
    private HorizontalLayout r1;
    private VerticalLayout c2;
    private FormLayout frm;
    private HorizontalLayout r3;

    private Label lblLeermodule;
    private TextField txtLeermoduleSearch;

    private Grid<LeermoduleDTO> grid;

    private Label lblID;
    private TextField code;
    private TextField beschrijving;
    private TextField schooljaar;
    private Button btnCancel;
    private Button btnCreate;
    private Button btnUpdate;
    private Button btnDelete;

    @PostConstruct
    public void buildUI() {
        this.setSizeFull();

        splitLayout = new SplitLayout();
        splitLayout.setSizeFull();
        splitLayout.addToPrimary(createGridLayout());
        splitLayout.addToSecondary(createEditorLayout());
        add(splitLayout);
    }

    private Component createGridLayout() {
        c1 = new VerticalLayout();
        c1.setWidthFull();

        r1 = new HorizontalLayout();
        lblLeermodule = new Label("Code (bevat)");
        txtLeermoduleSearch = new TextField();

        txtLeermoduleSearch.setValueChangeMode(ValueChangeMode.EAGER);
        txtLeermoduleSearch.addValueChangeListener(e -> handleClickSearch(null));

        r1.add(lblLeermodule);
        r1.add(txtLeermoduleSearch);


        grid = new Grid<>();
        grid.setItems(new ArrayList<LeermoduleDTO>(0));
        grid.addColumn(LeermoduleDTO::getCode).setHeader("Code").setSortable(true);
        grid.addColumn(LeermoduleDTO::getBeschrijving).setHeader("Beschrijving").setSortable(true);
        grid.addColumn(LeermoduleDTO::getSchooljaar).setHeader("Schooljaar").setSortable(true);
        grid.setHeightFull();

        //when a row is selected or deselected, populate form
        grid.asSingleSelect().addValueChangeListener(event -> populateForm(event.getValue()));

        c1.add(r1);
        c1.add(grid);
        c1.setWidth("75%");
        return c1;
    }

    private Component createEditorLayout() {
        c2 = new VerticalLayout();

        lblID = new Label("");

        code = new TextField();
        code.setRequired(true);
        code.setMaxLength(128);
        code.setErrorMessage("Verplicht veld!");

        beschrijving = new TextField();
        beschrijving.setRequired(true);
        beschrijving.setMaxLength(128);
        beschrijving.setErrorMessage("Verplicht veld!");

        schooljaar = new TextField();
        schooljaar.setRequired(true);
        schooljaar.setMaxLength(128);
        schooljaar.setErrorMessage("Verplicht veld!");

        frm = new FormLayout();
        frm.addFormItem(code, "Code");
        frm.addFormItem(beschrijving, "Beschrijving");
        frm.addFormItem(schooljaar, "Schooljaar");

        r3 = new HorizontalLayout();
        r3.setWidthFull();
        r3.setSpacing(true);

        btnCancel = new Button("Annuleren");
        btnCancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        btnCancel.addClickListener(e -> handleClickCancel(e));

        btnCreate = new Button("Toevoegen");
        btnCreate.addClickListener(e -> handleClickCreate(e));

        btnUpdate = new Button("Opslaan");
        btnUpdate.addClickListener(e -> handleClickUpdate(e));
        btnUpdate.setVisible(false);

        btnDelete = new Button("Verwijderen");
        btnDelete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        btnDelete.addClickListener(e -> handleClickDelete(e));
        btnDelete.setVisible(false);

        r3.add(btnCancel, btnCreate, btnUpdate, btnDelete);

        c2.add(frm);
        c2.add(r3);
        c2.setWidth("25%");

        return c2;
    }

    @Override
    // When anything automatic needs to happen just before the page is shown
    // Such as the loading of data
    public void afterNavigation(AfterNavigationEvent event) {
        grid.setItems(leermoduleEJBLocal.getAllLeermodules());
    }

    private void handleClickSearch(ClickEvent event) {
        if (txtLeermoduleSearch.getValue().trim().length() == 0) {
            grid.setItems(leermoduleEJBLocal.getAllLeermodules());
        } else {
            grid.setItems(leermoduleEJBLocal.getLeermodules(txtLeermoduleSearch.getValue().trim()));
        }
    }

    private void handleClickCancel(ClickEvent event) {
        grid.asSingleSelect().clear();
        resetForm();
    }

    private void handleClickCreate(ClickEvent event) {
        if (!isformValid()){
            Notification.show("Er zijn validatiefouten", 3000, Notification.Position.MIDDLE);
            return;
        }
        LeermoduleDTO s = new LeermoduleDTO(0L, code.getValue(), beschrijving.getValue(), schooljaar.getValue());
        try {
            long i = leermoduleEJBLocal.createLeermodule(s);

            Notification.show("Leermodule created (id: " + i + ")", 3000, Notification.Position.TOP_CENTER);
            handleClickSearch(null);
            resetForm();
        } catch (EJBException e) {
            Notification.show(e.getCausedByException().getMessage(), 5000, Notification.Position.TOP_CENTER);
        }
    }

    private void handleClickUpdate(ClickEvent event) {
        if (!isformValid()){
            Notification.show("Er zijn validatiefouten", 3000, Notification.Position.MIDDLE);
            return;
        }
        LeermoduleDTO s = new LeermoduleDTO(Integer.parseInt(lblID.getText()), code.getValue(),beschrijving.getValue(), schooljaar.getValue());
        try {
            leermoduleEJBLocal.updateLeermodule(s);

            Notification.show("Leermodule aangepast", 3000, Notification.Position.TOP_CENTER);
            handleClickSearch(null);
            resetForm();
        } catch (EJBException e) {
            Notification.show(e.getCausedByException().getMessage(), 5000, Notification.Position.TOP_CENTER);
        }
    }

    private void handleClickDelete(ClickEvent event) {
        try {
            leermoduleEJBLocal.deleteLeermodule(Integer.parseInt(lblID.getText()));

            Notification.show("Leermodule verwijderd", 3000, Notification.Position.TOP_CENTER);
            handleClickSearch(null);
            resetForm();
        } catch (EJBException e) {
            Notification.show(e.getCausedByException().getMessage(), 5000, Notification.Position.TOP_CENTER);
        }
    }

    private void populateForm(LeermoduleDTO s) {
        btnCreate.setVisible(false);
        btnUpdate.setVisible(true);
        btnDelete.setVisible(true);

        if (s != null) {
            lblID.setText("" + s.getId());
            if (s.getCode() != null) {
                code.setValue(s.getCode());
            } else {
                code.setValue("");
            }
            if (s.getBeschrijving() != null) {
                beschrijving.setValue(s.getBeschrijving());
            } else {
                beschrijving.setValue("");
            }

            if (s.getSchooljaar() != null) {
                schooljaar.setValue(s.getSchooljaar());
            } else {
                schooljaar.setValue("");
            }
        }
    }

    private void resetForm(){
        lblID.setText("");
        code.clear();
        code.setInvalid(false);
        beschrijving.clear();
        beschrijving.setInvalid(false);
        schooljaar.clear();
        schooljaar.setInvalid(false);

        btnCreate.setVisible(true);
        btnUpdate.setVisible(false);
        btnDelete.setVisible(false);
    }

    private boolean isformValid(){
        boolean result = true;
        if (code.getValue() == null) {
            code.setInvalid(true);
            result = false;
        }
        if (code.getValue().trim().length() == 0) {
            code.setInvalid(true);
            result = false;
        }
        if (beschrijving.getValue() == null) {
            beschrijving.setInvalid(true);
            result = false;
        }
        if (beschrijving.getValue().trim().length() == 0) {
            beschrijving.setInvalid(true);
            result = false;
        }
        if (schooljaar.getValue().trim().length() == 0 || schooljaar.getValue() == null) {
            schooljaar.setInvalid(true);
            result = false;
        }
        return result;
    }
}
