package be.ucll.java.ent.view;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

@Route("hello")
@Theme(value = Lumo.class, variant = Lumo.LIGHT)
@PageTitle("Hello Vaadin")
public class HelloVaadinView extends VerticalLayout {

    private Text txtHello;
    private TextField txtName;
    private Button btnHello;
    private Button btnNav;

    public HelloVaadinView() {
        txtHello = new Text("Welkom op de Vaadin demonstratie pagina");
        add(txtHello);

        txtName = new TextField("Uw naam");
        // De Tooltip met een zinnetje info over wat je moet gaan doen
        txtName.setTitle("Vul in dit tekstveld uw naam in");
        // een 'hint'
        txtName.setPlaceholder("Uw naam ph");
        // een verificatie op de maximale lengte
        txtName.setMaxLength(128);

        txtName.setRequired(true);
        txtName.setRequiredIndicatorVisible(true);

        add(txtName);

        btnHello = new Button("Tell me your name");
        btnHello.addClickListener(event -> {
            if (txtName.getValue().trim().length() == 0){
                txtName.setErrorMessage("U bent verplicht een naam in te vullen");
                txtName.setInvalid(true);
            } else {
                Notification.show("Hello " + txtName.getValue(), 2000, Notification.Position.TOP_STRETCH);
            }
        });
        add(btnHello);

        // Pure HTML
        add(new Hr());
        Span spn = new Span(new Text("Voorbeeldje"), new Html("<sup style='color: green'>***</sup>"));
        add(spn);

        // Layout in Layout
        HorizontalLayout hl = new HorizontalLayout();

        Button btn = new Button("BBB");
        Checkbox ckb = new Checkbox("I'm a professional Vaadin developer");
        Details det = new Details("Algemene tekst", new Text("Dit zijn alle details alleen zichtbaar wanneer uitgeklapt"));

        hl.add(btn, ckb, det);

        hl.setWidthFull();
        hl.setJustifyContentMode(JustifyContentMode.EVENLY);
        hl.setAlignItems(Alignment.BASELINE);

        add(hl);

        btnNav = new Button("Navigate to 'student'");
        btnNav.addClickListener(e -> btnNav.getUI().ifPresent((ui -> ui.navigate("student"))));
        add(btnNav);
    }
}
